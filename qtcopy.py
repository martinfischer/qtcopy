import sys
import glob
import os
import shutil
import stagger
from PyQt4 import QtCore, QtGui

from qtcopy_ui import Ui_MainWindow

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


class QtCopy(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        QtCore.QObject.connect(self.ui.toolButton,
                               QtCore.SIGNAL("clicked()"), self.askfolder)
        QtCore.QObject.connect(self.ui.toolButton_2,
                               QtCore.SIGNAL("clicked()"), self.askfolder2)
        QtCore.QObject.connect(self.ui.toolButton_3,
                               QtCore.SIGNAL("clicked()"), self.askfolder3)
        QtCore.QObject.connect(self.ui.checkBox,
                               QtCore.SIGNAL(_fromUtf8("stateChanged(int)")),
                               self.toggleDropbox)
        QtCore.QObject.connect(self.ui.actionAbout,
                               QtCore.SIGNAL(_fromUtf8("activated()")),
                               self.about)
        QtCore.QObject.connect(self.ui.pushButton_3,
                               QtCore.SIGNAL(_fromUtf8("clicked()")),
                               self.reset)
        QtCore.QObject.connect(self.ui.pushButton,
                               QtCore.SIGNAL(_fromUtf8("clicked()")),
                               self.kopieren)
        QtCore.QObject.connect(self.ui.pushButton_2,
                               QtCore.SIGNAL(_fromUtf8("clicked()")),
                               self.delete_dirs)

    def askfolder(self):
        folder = str(QtGui.QFileDialog.getExistingDirectory(self,
                                                        "Select Directory"))
        if folder:
            self.ui.lineEdit.setText(_fromUtf8(folder))

    def askfolder2(self):
        folder = str(QtGui.QFileDialog.getExistingDirectory(self,
                                                        "Select Directory"))
        if folder:
            self.ui.lineEdit_2.setText(_fromUtf8(folder))

    def askfolder3(self):
        folder = str(QtGui.QFileDialog.getExistingDirectory(self,
                                                        "Select Directory"))
        if folder:
            self.ui.lineEdit_3.setText(_fromUtf8(folder))

    def toggleDropbox(self):
        if not self.ui.checkBox.isChecked():
            self.ui.lineEdit_3.hide()
            self.ui.toolButton_3.hide()
            self.ui.label_3.hide()
        else:
            self.ui.lineEdit_3.show()
            self.ui.toolButton_3.show()
            self.ui.label_3.show()

    def about(self):
        self.ui.plainTextEdit.setPlainText(_fromUtf8("qtcopy\n\n"
                                           "Autor: Martin Fischer\n\n"
                                            "E-Mail: martin@fischerweb.net\n\n"
                                            "Website: martinfischer.name"))

    def reset(self):
        self.ui.plainTextEdit.setPlainText(_fromUtf8("Hallo!\n"
                                           "\n"
"Wähle einen Ausgangsordner mit Unterordnern mit Dateien und einen Zielordner "
"wohin diese Dateien verschoben werden sollen.\n"
"\n"
"Mp3 Dateien (Podcasts) werden direkt in die Dropbox verschoben und der ID3 "
"Tag wird leicht verändert.\n"
"\n"
"Diese Software ist gedacht, um Miro Content in einen anderen Ordner zu "
"verschieben und Podcasts Android-Dropbox-bereit zu machen."))
        self.ui.lineEdit.setText(_fromUtf8("/home/martin/Videos/Miro"))
        self.ui.lineEdit_2.setText(_fromUtf8("/home/martin/Videos/TV/C Tube"))
        self.ui.checkBox.setChecked(True)
        self.ui.lineEdit_3.setText(_fromUtf8("/home/martin/Dropbox/radio"))

    def kopieren(self):
        self.ui.plainTextEdit.setPlainText(_fromUtf8(""))
        src = self.ui.lineEdit.text() + "/*"
        dst = self.ui.lineEdit_2.text()

        self.files = glob.glob(os.path.join(src, "*.*"))
        self.thread = Worker(dst, self.ui.lineEdit_3.text(),
                             self.ui.checkBox.isChecked(), self.files)
        self.thread.kopiert.connect(self.on_ready)
        self.thread.start()

    def on_ready(self, text):
        self.ui.plainTextEdit.appendPlainText(_fromUtf8(text))

    def delete_dirs(self):
        reply = QtGui.QMessageBox.question(self, _fromUtf8("Ordner löschen"),
            "Bist du sicher?", QtGui.QMessageBox.Yes |
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)
        src = self.ui.lineEdit.text()

        if reply == QtGui.QMessageBox.Yes:
            self.ui.plainTextEdit.setPlainText(_fromUtf8(""))
            dirs = glob.glob(src + "/*")
            for dirname in dirs:
                dirname = os.path.normpath(dirname)
                if dirname == os.path.join(src, "Incomplete Downloads"):
                    continue
                else:
                    shutil.rmtree(dirname)
                    self.ui.plainTextEdit.appendPlainText(_fromUtf8(
                                                        dirname + " gelöscht"))
            if len(dirs) > 1:
                self.ui.plainTextEdit.appendPlainText(_fromUtf8(
                                                      "{}\n".format("*" * 80)))
                self.ui.plainTextEdit.appendPlainText(_fromUtf8(
                                                      "Ordner gelöscht\n"))
                self.ui.plainTextEdit.appendPlainText(_fromUtf8(
                                                      "{}\n".format("*" * 80)))
            else:
                self.ui.plainTextEdit.appendPlainText(_fromUtf8(
                                                      "{}\n".format("*" * 80)))
                self.ui.plainTextEdit.appendPlainText(_fromUtf8(
                                                "Keine Ordner zu löschen\n"))
                self.ui.plainTextEdit.appendPlainText(_fromUtf8(
                                                      "{}\n".format("*" * 80)))


class Worker(QtCore.QThread):
    kopiert = QtCore.pyqtSignal(object)

    def __init__(self, dst, dropbox, checked, files):
        QtCore.QThread.__init__(self)
        self.dst = dst
        self.files = files
        self.dropbox = dropbox
        self.checked = checked

    def run(self):
        for filename in self.files:
            self.filename = os.path.realpath(filename)
            if self.filename.split(".")[-1] == "mp3" and self.checked:
                try:
                    tag = stagger.read_tag(self.filename)
                    old_tag = tag.album
                    self.kopiert.emit(
            "--Ändere mp3 ID3 von \"{}\" auf \"Podcast\"\n".format(old_tag))
                    tag.album = "Podcast"
                    tag.write()

                    if tag.title:
                        self.new_filename = ""
                        for i in self.filename.split("/")[:-1]:
                            self.new_filename += i + "/"
                        self.new_filename += tag.title + ".mp3"
                        os.rename(self.filename, self.new_filename)

                        self.kopiert.emit("Kopiere: {}\n".format(
                                    self.new_filename.split("/")[-1]))
                        shutil.copy(self.new_filename, self.dropbox)
                    else:
                        self.kopiert.emit("Kopiere: {}\n".format(
                                            self.filename.split("/")[-1]))
                        shutil.copy(self.filename, self.dropbox)

                except Exception as err:
                    self.kopiert.emit("---Error bei {}: {}\n".format(
                                        self.filename.split("/")[-1],
                                            err))
                    try:
                        self.kopiert.emit("Kopiere: {}\n".format(
                                            self.filename.split("/")[-1]))
                        shutil.copy(self.filename, self.dropbox)
                    except Exception as err2:
                        self.kopiert.emit("---Error bei {}: {}\n".format(
                                                self.filename.split("/")[-1],
                                                err2))

            else:
                self.kopiert.emit("Kopiere: {}\n".format(
                                            self.filename.split("/")[-1]))
                shutil.copy(self.filename, self.dst)

        if self.files:
            self.kopiert.emit("{}\n".format("*" * 80))
            self.kopiert.emit("Kopieren fertig\n")
            self.kopiert.emit("{}\n".format("*" * 80))
        else:
            self.kopiert.emit("{}\n".format("*" * 80))
            self.kopiert.emit("Nichts zu kopieren\n")
            self.kopiert.emit("{}\n".format("*" * 80))


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    qtcopy = QtCopy()
    qtcopy.show()
    sys.exit(app.exec_())
