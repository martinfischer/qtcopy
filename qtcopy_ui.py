# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(640, 480)
        MainWindow.setWindowTitle(_fromUtf8("qtcopy"))
        MainWindow.setToolTip(_fromUtf8(""))
        MainWindow.setStatusTip(_fromUtf8(""))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setToolTip(_fromUtf8(""))
        self.centralwidget.setStatusTip(_fromUtf8(""))
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.pushButton = QtGui.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(10, 420, 120, 27))
        self.pushButton.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.pushButton.setToolTip(_fromUtf8("Dateien kopieren"))
        self.pushButton.setStatusTip(_fromUtf8(""))
        self.pushButton.setText(_fromUtf8("Kopieren"))
        self.pushButton.setIconSize(QtCore.QSize(16, 16))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(160, 420, 120, 27))
        self.pushButton_2.setToolTip(_fromUtf8("Ordner löschen"))
        self.pushButton_2.setStatusTip(_fromUtf8(""))
        self.pushButton_2.setText(_fromUtf8("Löschen"))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_3 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(350, 420, 120, 27))
        self.pushButton_3.setToolTip(_fromUtf8("Alles zurücksetzen"))
        self.pushButton_3.setStatusTip(_fromUtf8(""))
        self.pushButton_3.setText(_fromUtf8("Reset"))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_4 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(500, 420, 120, 27))
        self.pushButton_4.setToolTip(_fromUtf8("Programm schließen"))
        self.pushButton_4.setStatusTip(_fromUtf8(""))
        self.pushButton_4.setText(_fromUtf8("Schließen"))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.toolButton = QtGui.QToolButton(self.centralwidget)
        self.toolButton.setGeometry(QtCore.QRect(260, 320, 23, 25))
        self.toolButton.setToolTip(_fromUtf8("Ordner wählen"))
        self.toolButton.setStatusTip(_fromUtf8(""))
        self.toolButton.setText(_fromUtf8("..."))
        self.toolButton.setObjectName(_fromUtf8("toolButton"))
        self.toolButton_2 = QtGui.QToolButton(self.centralwidget)
        self.toolButton_2.setGeometry(QtCore.QRect(600, 320, 23, 25))
        self.toolButton_2.setToolTip(_fromUtf8("Ordner wählen"))
        self.toolButton_2.setStatusTip(_fromUtf8(""))
        self.toolButton_2.setText(_fromUtf8("..."))
        self.toolButton_2.setObjectName(_fromUtf8("toolButton_2"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 290, 251, 31))
        self.label.setToolTip(_fromUtf8(""))
        self.label.setStatusTip(_fromUtf8(""))
        self.label.setText(_fromUtf8("Ausgangsordner"))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(350, 290, 251, 31))
        self.label_2.setToolTip(_fromUtf8(""))
        self.label_2.setStatusTip(_fromUtf8(""))
        self.label_2.setText(_fromUtf8("Zielordner"))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.lineEdit = QtGui.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(10, 320, 250, 27))
        self.lineEdit.setToolTip(_fromUtf8(""))
        self.lineEdit.setStatusTip(_fromUtf8(""))
        self.lineEdit.setReadOnly(True)
        self.lineEdit.setText(_fromUtf8("/home/martin/Videos/Miro"))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.lineEdit_2 = QtGui.QLineEdit(self.centralwidget)
        self.lineEdit_2.setGeometry(QtCore.QRect(350, 320, 250, 27))
        self.lineEdit_2.setToolTip(_fromUtf8(""))
        self.lineEdit_2.setStatusTip(_fromUtf8(""))
        self.lineEdit_2.setReadOnly(True)
        self.lineEdit_2.setText(_fromUtf8("/home/martin/Videos/TV/C Tube"))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.plainTextEdit = QtGui.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(10, 10, 621, 281))
        self.plainTextEdit.setAcceptDrops(False)
        self.plainTextEdit.setToolTip(_fromUtf8(""))
        self.plainTextEdit.setStatusTip(_fromUtf8(""))
        self.plainTextEdit.setReadOnly(True)
        self.plainTextEdit.setPlainText(_fromUtf8("Hallo!\n"
"\n"
"Wähle einen Ausgangsordner mit Unterordnern mit Dateien und einen Zielordner "
"wohin diese Dateien verschoben werden sollen.\n"
"\n"
"Mp3 Dateien (Podcasts) werden direkt in die Dropbox verschoben und der ID3 "
"Tag wird leicht verändert.\n"
"\n"
"Diese Software ist gedacht, um Miro Content in einen anderen Ordner zu "
"verschieben und Podcasts Android-Dropbox-bereit zu machen."))
        self.plainTextEdit.setObjectName(_fromUtf8("plainTextEdit"))
        self.checkBox = QtGui.QCheckBox(self.centralwidget)
        self.checkBox.setGeometry(QtCore.QRect(10, 382, 281, 22))
        self.checkBox.setToolTip(_fromUtf8(
                        "Der Album ID3-Tag wird auf \"Podcast\" geändert."))
        self.checkBox.setStatusTip(_fromUtf8(""))
        self.checkBox.setText(_fromUtf8("mp3s in die Dropbox und Tag ändern?"))
        self.checkBox.setChecked(True)
        self.checkBox.setObjectName(_fromUtf8("checkBox"))
        self.lineEdit_3 = QtGui.QLineEdit(self.centralwidget)
        self.lineEdit_3.setGeometry(QtCore.QRect(350, 380, 250, 27))
        self.lineEdit_3.setToolTip(_fromUtf8(""))
        self.lineEdit_3.setStatusTip(_fromUtf8(""))
        self.lineEdit_3.setReadOnly(True)
        self.lineEdit_3.setText(_fromUtf8("/home/martin/Dropbox/radio"))
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.toolButton_3 = QtGui.QToolButton(self.centralwidget)
        self.toolButton_3.setGeometry(QtCore.QRect(600, 380, 23, 25))
        self.toolButton_3.setToolTip(_fromUtf8("Ordner wählen"))
        self.toolButton_3.setStatusTip(_fromUtf8(""))
        self.toolButton_3.setText(_fromUtf8("..."))
        self.toolButton_3.setObjectName(_fromUtf8("toolButton_3"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(350, 350, 251, 31))
        self.label_3.setToolTip(_fromUtf8(""))
        self.label_3.setStatusTip(_fromUtf8(""))
        self.label_3.setText(_fromUtf8("Dropbox Zielordner"))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 640, 25))
        self.menubar.setToolTip(_fromUtf8(""))
        self.menubar.setStatusTip(_fromUtf8(""))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setToolTip(_fromUtf8(""))
        self.menuFile.setStatusTip(_fromUtf8(""))
        self.menuFile.setTitle(_fromUtf8("File"))
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setToolTip(_fromUtf8(""))
        self.menuHelp.setStatusTip(_fromUtf8(""))
        self.menuHelp.setTitle(_fromUtf8("Help"))
        self.menuHelp.setObjectName(_fromUtf8("menuHelp"))
        MainWindow.setMenuBar(self.menubar)
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setText(_fromUtf8("Exit"))
        self.actionExit.setIconText(_fromUtf8("Exit"))
        self.actionExit.setToolTip(_fromUtf8("Exit"))
        self.actionExit.setObjectName(_fromUtf8("actionExit"))
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setText(_fromUtf8("About"))
        self.actionAbout.setIconText(_fromUtf8("About"))
        self.actionAbout.setToolTip(_fromUtf8("About"))
        self.actionAbout.setObjectName(_fromUtf8("actionAbout"))
        self.menuFile.addAction(self.actionExit)
        self.menuHelp.addAction(self.actionAbout)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        QtCore.QObject.connect(self.actionExit,
                               QtCore.SIGNAL(_fromUtf8("activated()")),
                               MainWindow.close)
        QtCore.QObject.connect(self.pushButton_4,
                               QtCore.SIGNAL(_fromUtf8("clicked()")),
                               MainWindow.close)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
